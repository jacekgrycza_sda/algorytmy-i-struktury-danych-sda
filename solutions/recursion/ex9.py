# Napisz rekurencyjnie funkcję sprawdzającą czy podany wyraz jest palindromem

def is_palindrome(input):
    pass


assert is_palindrome('ala') is True
assert is_palindrome('alaa') is False
assert is_palindrome('A') is True
assert is_palindrome('ala ma kota') is False
assert is_palindrome('zakopanenapokaz') is True
