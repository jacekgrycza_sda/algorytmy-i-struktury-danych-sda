# Napisz rekurencyjną funkcję obracającą stringa.

def reverse(input):
    if len(input) == 0:
        return input
    return input[-1] + reverse(input[:-1])


assert reverse('abcde') == 'edcba'
assert reverse('A') == 'A'
assert reverse('') == ''
