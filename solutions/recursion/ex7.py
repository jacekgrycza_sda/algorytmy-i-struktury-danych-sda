# Napisz rekurencyjnie funckję sumującą cyfry podanej liczby całkowitej dodatniej.


def sum_digits(n):
    if n < 10:
        return n
    return sum_digits(n // 10) + n % 10


assert sum_digits(1111) == 4
assert sum_digits(12345) == 15
assert sum_digits(22222222) == 16
