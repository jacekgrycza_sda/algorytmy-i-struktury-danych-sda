#  Napisz rekurencyjnie funkcję przyjmującą jako argument liczbę całkowitą dodatnią n i zwracającą sumę liczb od 1 do n

def sum_n(n):
    if n == 1:
        return 1
    return n + sum_n(n - 1)


assert sum_n(5) == 15
assert sum_n(10) == 55
assert sum_n(1) == 1
