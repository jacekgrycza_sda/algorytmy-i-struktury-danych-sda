# Napisz funkcję przyjmującą jako argument dwie liczby całkowite nieujemne i zwracającą ich iloczyn, funkcja ma być rekurencyjna.
# Nie wykorzystuj operatora *


def multiply(n, m):
    if n == 0 or m == 0:
        return 0
    if n < m:
        return multiply(n - 1, m) + m
    else:
        return multiply(n, m - 1) + n



assert multiply(5, 50) == 250
assert multiply(0, 0) == 0
assert multiply(123, 0) == 0
assert multiply(123, 1) == 123
assert multiply(1, 321) == 321
