# Napisz rekurencyjną funkcję obracającą cyfry w liczbie całkowitej dodatniej


def reverse_digits(n):
    return reverse_digits_helper(n, 0)


def reverse_digits_helper(n, current_value):
    if n < 10:
        return current_value * 10 + n
    else:
        last_digit = n % 10
        next_current_value = current_value * 10 + last_digit
        n = n // 10
        return reverse_digits_helper(n, next_current_value)


assert reverse_digits(12345) == 54321
assert reverse_digits(111) == 111
assert reverse_digits(468) == 864
