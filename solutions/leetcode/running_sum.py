# https://leetcode.com/problems/running-sum-of-1d-array/
from typing import List


class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        for i in range(1, len(nums)):
            nums[i] = nums[i - 1] + nums[i]
        return nums


solution = Solution()
assert solution.runningSum([1, 2, 3, 4]) == [1, 3, 6, 10]
assert solution.runningSum([1, 1, 1, 1, 1]) == [1, 2, 3, 4, 5]
