# https://leetcode.com/problems/two-sum/
from typing import List


class Solution1:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(len(nums)):
            for j in range(len(nums)):
                if i == j: continue
                if nums[i] + nums[j] == target:
                    return [i, j]


class Solution2:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]


class Solution3:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        temp_pairs = {}
        for i in range(len(nums)):
            if nums[i] not in temp_pairs:
                temp_pairs[target - nums[i]] = i
            else:
                return [i, temp_pairs[nums[i]]]


solution = Solution1()

assert solution.twoSum([2, 7, 11, 15], 9) in ([0, 1], [1, 0])
assert solution.twoSum([3, 2, 4], 6) in ([1, 2], [2, 1])
assert solution.twoSum([3, 3], 6) in ([0, 1], [1, 0])

# jaka zlozonosc czasowa i pamięciowa poszczegolnych rozwiazan?
