class Solution:
    def isPowerOfThree(self, n: int) -> bool:
        while n > 1:
            rem = n % 3
            if rem != 0: return False
            n = n // 3
        return True


solution = Solution()
assert solution.isPowerOfThree(81)
assert not solution.isPowerOfThree(82)
assert solution.isPowerOfThree(9)
