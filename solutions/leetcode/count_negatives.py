# https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/
import bisect
from typing import List


class Solution:
    def countNegatives(self, grid: List[List[int]]) -> int:
        result = 0
        grid = grid[::-1]
        for i in range(len(grid)):
            grid[i] = grid[i][::-1]
        end = len(grid[0])
        for array in grid:
            leftmost_place_for_zero = bisect.bisect_left(array, 0, hi=end)
            result += leftmost_place_for_zero
            end = leftmost_place_for_zero
            if end == 0: break
        return result


solution = Solution()
assert solution.countNegatives([[4, 3, 2, -1], [3, 2, 1, -1], [1, 1, -1, -2], [-1, -1, -2, -3]]) == 8
