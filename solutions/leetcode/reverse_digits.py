# https://leetcode.com/problems/reverse-integer/
class Solution:
    def reverse(self, x: int) -> int:
        stack = list()
        negative = x < 0
        x = abs(x)
        while x > 0:
            stack.append(x % 10)
            x = x // 10
        k = 0
        while stack:
            x += 10 ** k * stack.pop()
            k += 1
        if negative:
            x = -x
        if x > 2147483648 - 1 or x < -2147483648:
            return 0
        return x


solution = Solution()
assert solution.reverse(1534236469) == 0
assert solution.reverse(-654321) == -123456
assert solution.reverse(-123) == -321
assert solution.reverse(6543) == 3456
