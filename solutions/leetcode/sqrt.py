# https://leetcode.com/problems/sqrtx/

class Solution:
    def mySqrt(self, x: int) -> int:
        return self.binary_sqrt_search(x, 0, x)

    def binary_sqrt_search(self, x, lo, hi):
        mid = (lo + hi) // 2
        if mid ** 2 <= x < (mid + 1) ** 2:
            return mid
        elif mid ** 2 > x:
            return self.binary_sqrt_search(x, lo, mid - 1)
        else:
            return self.binary_sqrt_search(x, mid + 1, hi)


class Solution2:
    def mySqrt(self, x: int) -> int:
        left, right = 0, x
        while left <= right:
            mid = left + (right - left) // 2
            if mid * mid <= x < (mid + 1) * (mid + 1):
                return mid
            elif x < mid * mid:
                right = mid - 1
            else:
                left = mid + 1


solution = Solution2()
assert solution.mySqrt(120) == 10
assert solution.mySqrt(144) == 12
assert solution.mySqrt(148) == 12
assert solution.mySqrt(2500) == 50
assert solution.mySqrt(0) == 0
