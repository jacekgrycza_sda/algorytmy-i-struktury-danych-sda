# Zaimplementuj przeszukiwanie binarne
import time


# O(logn)
def binary_search(lst, element):
    """

    :param lst:  list of elements to search, assume lst is sorted
    :param element: element to find
    :return: index of the element or -1 if element is not present in the lst
    """
    return binary_search_helper(lst, element, 0, len(lst) - 1)


def binary_search_helper(lst, element, left, right):
    mid_idx = (left + right) // 2
    if left <= right:
        if lst[mid_idx] == element:
            return mid_idx
        elif lst[mid_idx] < element:
            return binary_search_helper(lst, element, mid_idx + 1, right)
        else:
            return binary_search_helper(lst, element, left, mid_idx - 1)
    return -1


# O(n)
def linear_search(lst, element):
    for idx, el in enumerate(lst):
        if el == element:
            return idx
    return -1


def binary_search_iter(lst, element):
    low = 0
    high = len(lst) - 1
    while low <= high:
        mid = (low + high) // 2
        if lst[mid] == element:
            return mid
        elif lst[mid] < element:
            low = mid + 1
        else:
            high = mid - 1

    return -1


big_list = list(range(1000000))
num_to_find = 678678
iterations = 1000

bs_start = time.time()
for _ in range(iterations):
    idx = binary_search(big_list, num_to_find)
# print(f'[BS] Idx of {num_to_find} is: {idx}')
print(f'BS time: {time.time() - bs_start}')

bs_start = time.time()
for _ in range(iterations):
    idx = binary_search_iter(big_list, num_to_find)
# print(f'[BS] Idx of {num_to_find} is: {idx}')
print(f'BS iter time: {time.time() - bs_start}')

# ls_start = time.time()
# for _ in range(iterations):
#     idx = linear_search(big_list, num_to_find)
# # print(f'[LS] Idx of {num_to_find} is: {idx}')
# print(f'LS time: {time.time() - ls_start}')

assert binary_search([31], 31) == 0
assert binary_search([31], 30) == -1
assert binary_search([], 30) == -1
assert binary_search([1, 2, 3, 30], 30) == 3


assert binary_search_iter([31], 31) == 0
assert binary_search_iter([31], 30) == -1
assert binary_search_iter([], 30) == -1
assert binary_search_iter([1, 2, 3, 30], 30) == 3
