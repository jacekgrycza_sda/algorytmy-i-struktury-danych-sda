import bisect

nums = [1, 2, 2, 5, 7, 8, 8, 9, 15]

# 1. Korzystając z biblioteki bisect, znajdź najmniejszy możliwy indeks pod który można wstawić liczbę 8 do tablicy nums, tak aby pozostała
# posortowana

print(f'Lowest possible index for 8 is: {bisect.bisect_left(nums, 8)}')

# 2. To samo tylko największy indeks

print(f'Highest possible index for 8 is: {bisect.bisect_right(nums, 8)}')
print(f'Highest possible index for 8 is: {bisect.bisect(nums, 8)}')

# 3. Wstaw przy użyciu tej biblioteki liczbę 8 na najmniejszy możliwy indeks, tak aby lista nums pozostała posortowana, wykorzystaj
# którąś z metod insort

bisect.insort_left(nums, 8)
print(nums)

# 4. Teraz wstaw 2 na najwyższy możliwy indeks, ale nie korzystaj z insorta

nums.insert(bisect.bisect_right(nums, 2), 2)
print(nums)

# 5. Na pewnym egzaminie ocena 2 jest za wynik procentowy w przedziale [0, 50], 3 za (50, 70], 4 za (70,90], 5 za (90, 100]
# Przy użyciu biblioteki bisect zaimplementuj funkcję grade, która dla zadanego wyniki zwróci ocenę

def grade(score, breakpoints=(50, 70, 90), grades=(2, 3, 4, 5)):
    idx = bisect.bisect_left(breakpoints, score)
    return grades[idx]


assert grade(50) == 2
assert grade(59.5) == 3
assert grade(70) == 3
assert grade(89) == 4
assert grade(91) == 5


def binary_search_iter(lst, element):
    # ustawic poczatkowe wartosci left i right
    # dopoki left <= right liczymy indeks srodkowy i sprawdzamy tam wartosc
    # jesli sie zgadza to zwracamy
    # jest ta wartosc jest wieksza to sprawdzamy lewa czesc tablicy, czyli ustawiamy odpowiednio right
    # jest mniejsza to odpowiednio zmieniamy left
    # zwracamy -1 jesli nie znajdziemy szukanego elementu
    pass

