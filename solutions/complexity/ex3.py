# Jaka jest złożoność czasowa i pamięciowa poniższego kodu w zależności od N?
import random

N = int(input('Podaj N: '))
result = 0
results_history = []
for i in range(N):
    for j in range(N, i - 1, -1):
        result += i + random.randint(50, 100)
    results_history.append(result)

print(f'result is {result}')
print(f'result is {results_history}')

# czasowa O(N^2), pamięciowa O(N)
