# Jaka jest złożoność czasowa i pamięciowa poniższego kodu w zależności od N?

N = int(input('Podaj N: '))
result = 0
for i in range(N):
    for j in range(N):
        result += i + j
print(f'result is {result}')

# Czasowa O(N^2), pamięciowa O(1)
