# Jaka jest złożoność czasowa i pamięciowa poniższego kodu w zależności od N i M?

N = int(input('Podaj N: '))
M = int(input('Podaj M: '))
result = 0
for i in range(N):
    result += i

for i in range(M):
    result += i

print(f'result is {result}')

# Czasowa O(N+M), pamięciowa O(1)
