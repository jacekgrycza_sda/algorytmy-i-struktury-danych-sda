# Jaka jest złożoność czasowa i pamięciowa poniższego kodu w zależności od N?
import random

N = int(input('Podaj N: '))
result = 0
for i in range(N):
    j = 1
    while j <= N:
        result += i + j + random.randint(5, 10)
        j *= 2
print(f'result is {result}')

# czasowa O(N*logN), pamięciowa O(1)