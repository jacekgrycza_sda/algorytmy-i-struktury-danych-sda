# Obróć stringa przy pomocy stosu
from solutions.data_structures.my_stack import Stack


def reverse_string(input):
    stack = Stack()
    for ch in input:
        stack.push(ch)
    result = ''
    while stack.size() > 0:
        result += stack.pop()
    return result


print(reverse_string('ala ma kota'))
