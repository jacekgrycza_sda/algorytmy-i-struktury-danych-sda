# Napisz program, który dla podanego stringa sprawdzi, czy nawiasy "(" i ")" są dobrze pootwierane i pozamykane.
from solutions.data_structures.my_stack import Stack


def check_parenthesis(input):
    stack = Stack()
    for ch in input:
        if ch == '(':
            stack.push(ch)
        elif ch == ')':
            if stack.size() == 0: return False
            stack.pop()
    return stack.size() == 0


assert check_parenthesis("(2+2)*2")
assert not check_parenthesis(")222222(")
assert not check_parenthesis("(((pp))(")
assert check_parenthesis("ala*(22+(2-2))+(9+1)(9-1)")
assert not check_parenthesis("((())")
assert check_parenthesis("ala ma kota")
