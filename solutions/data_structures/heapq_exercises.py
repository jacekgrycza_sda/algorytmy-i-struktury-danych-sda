# 1. Przy pomocy biblioteki heapq zaimplementuj sortowanie heap sort malejące
import heapq


def heap_sort_desc(lst):
    heap = []
    for element in lst:
        heapq.heappush(heap, -element)
    return [-heapq.heappop(heap) for _ in range(len(heap))]


lst = [1, 4, 2, 1, 5, 3, 6]
lst_sorted = heap_sort_desc(lst)
assert lst_sorted == [6, 5, 4, 3, 2, 1, 1]

# 2. Przekształć listę lst do kopca

lst = [2, 1, 5, 6, 1, 8, 5, 11]
heapq.heapify(lst)
print(lst)
