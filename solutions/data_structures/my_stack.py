# Zaimplementuj stos, użyj listy


class Stack:
    def __init__(self):
        self._elements = list()

    def push(self, element):
        self._elements.append(element)

    def pop(self):
        try:
            return self._elements.pop()
        except IndexError:
            raise BaseException(f"Stack empty")

    def size(self):
        return len(self._elements)