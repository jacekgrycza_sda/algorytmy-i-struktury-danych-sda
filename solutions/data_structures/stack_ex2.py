# Napisz program, który dla podanego stringa sprawdzi, czy nawiasy okrągłe, kwadratowe i "curly" są poprawnie pootwierane i pozamykane.
from solutions.data_structures.my_stack import Stack


def check_parenthesis(input):
    stack = Stack()
    brackets_pairs = {
        '(': ')',
        '[': ']',
        '{': '}'
    }

    for ch in input:
        if ch in brackets_pairs:
            stack.push(ch)
        elif ch in brackets_pairs.values():
            if stack.size() < 1: return False
            bracket = stack.pop()
            if brackets_pairs[bracket] != ch:
                return False

    return stack.size() == 0


assert check_parenthesis("(2+2)*2")
assert not check_parenthesis(")222222(")
assert not check_parenthesis("(((pp))(")
assert check_parenthesis("ala*(22+(2-2))+(9+1)(9-1)")
assert not check_parenthesis("((())")
assert check_parenthesis("ala ma kota")
assert check_parenthesis("{{{}}}")
assert check_parenthesis("(){}[]")
assert check_parenthesis("aaaaa(dsadsa)(dsadsa)(!@#$%^&)({{[[[asdasd]]]}})asdasd")
assert not check_parenthesis("aaaaa((dsadsa)(dsadsa)(!@#$%^&)({{[[[asdasd]]]}})asdasd")
assert not check_parenthesis("({[]]})")
assert not check_parenthesis("{]}")
