def quick_sort(lst, lo, hi):
    if lo < hi:
        p = partition(lst, lo, hi)
        quick_sort(lst, lo, p - 1)
        quick_sort(lst, p + 1, hi)


def partition(lst, lo, hi):
    pivot = lst[hi]
    i = lo
    for j in range(lo, hi):
        if lst[j] < pivot:
            lst[i], lst[j] = lst[j], lst[i]
            i += 1
    lst[i], lst[hi] = lst[hi], lst[i]
    return i


def partition2(lst, lo, hi):
    pass


lst = [5, 1, 7, 8, 10, 2, 0]
quick_sort(lst, 0, len(lst) - 1)
print(lst)
