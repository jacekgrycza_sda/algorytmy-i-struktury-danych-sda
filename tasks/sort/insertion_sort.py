def insertion_sort(lst):
    for i in range(1, len(lst)):
        temp = lst[i]
        for j in range(i, 0, -1):
            if lst[j - 1] > temp:
                lst[j - 1], lst[j] = lst[j], lst[j - 1]
            else:
                break
    return lst


def ins_sor(lst):
    for i in range(1, len(lst)):
        key = lst[i]
        j = i - 1
        while j >= 0 and key < lst[j]:
            lst[j + 1] = lst[j]
            j -= 1
        lst[j + 1] = key
    return lst


lst = [64, 34, 25, 12, 22, 11, 90]
# lst = [1, 2, 3, 4, 5, 6]
print(ins_sor(lst))

lst = [5, 1, 7, 3, 9, 10, 2, -5]
print(insertion_sort(lst))
