def merge_sort(lst):
    if len(lst) < 2:
        return lst

    mid = len(lst) // 2
    left = merge_sort(lst[:mid])
    right = merge_sort(lst[mid:])
    result = merge_lists(left, right)
    return result


def merge_lists(left, right):
    i = 0
    j = 0
    result = []
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    while i < len(left):
        result.append(left[i])
        i += 1

    while j < len(right):
        result.append(right[j])
        j += 1

    return result


print(merge_sort([4, 1, 56, 100, 0, 5]))


def mer_sor(lst):
    if len(lst) < 2: return lst
    result = []
    mid = int(len(lst) / 2)
    left = mer_sor(lst[:mid])  # slice tylko na intcie możliwy
    right = mer_sor(lst[mid:])
    while (len(left) > 0) and (len(right) > 0):
        if left[0] > right[0]:
            result.append(right.pop(0))
        else:
            result.append(left.pop(0))
    result.extend(left + right)
    return result
lst = [64, 34, 25, 12, 22, 11, 90, 12, 11, 13, 5, 6]
# lst = [1, 2, 3, 4, 5, 6]
print(mer_sor(lst))

# O(n logn)