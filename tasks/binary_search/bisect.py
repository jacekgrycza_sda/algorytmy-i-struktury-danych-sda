import bisect

nums = [1, 2, 2, 5, 7, 8, 8, 9, 15]


# 1. Korzystając z biblioteki bisect, znajdź najmniejszy możliwy indeks pod który można wstawić liczbę 8 do tablicy nums, tak aby pozostała
# posortowana


# 2. To samo tylko największy indeks


# 3. Wstaw przy użyciu tej biblioteki liczbę 8 na najmniejszy możliwy indeks, tak aby lista nums pozostała posortowana, wykorzystaj
# którąś z metod insort


# 4. Teraz wstaw 2 na najwyższy możliwy indeks, ale nie korzystaj z insorta


# 5. Na pewnym egzaminie ocena 2 jest za wynik procentowy w przedziale [0, 50], 3 za (50, 70], 4 za (70,90], 5 za (90, 100]
# Przy użyciu biblioteki bisect zaimplementuj funkcję grade, która dla zadanego wyniki zwróci ocenę

def grade(score, breakpoints=(50, 70, 90), grades=(2, 3, 4, 5)):
    pass


assert grade(50) == 2
assert grade(59.5) == 3
assert grade(70) == 3
assert grade(89) == 4
assert grade(91) == 5
