# Zaimplementuj przeszukiwanie binarne

def binary_search(lst, element):
    """

    :param lst:  list of elements to search, assume lst is sorted
    :param element: element to find
    :return: index of the element or -1 if element is not present in the lst
    """
    pass


def binary_search_helper(lst, element, left, right):
    pass


assert binary_search([31], 31) == 0
assert binary_search([31], 30) == -1
assert binary_search([], 30) == -1
assert binary_search([1, 2, 3, 30], 30) == 3
