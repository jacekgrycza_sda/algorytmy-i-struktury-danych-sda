# Napisz rekurencyjnie funkcję obliczającą n-ty wyraz ciągu Fibonacciego

def fibonacci(n):
    pass


assert fibonacci(0) == 0
assert fibonacci(1) == 1
assert fibonacci(3) == 2
assert fibonacci(8) == 21
assert fibonacci(12) == 144
