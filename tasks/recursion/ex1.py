def function(n):
    if n == 0:
        return 0
    function(n - 1)
    print(n)


# Jaki będzie wynik poniższego wywołania?
# function(5)


def function2(n):
    print(n)
    if n == 0:
        return 0
    function2(n - 1)

# A tego?
# function2(5)

# Co się stanie po wykonaniu poniższej linijki?
# function(-5)
