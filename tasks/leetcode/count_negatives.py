# https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/
from typing import List


class Solution:
    def countNegatives(self, grid: List[List[int]]) -> int:
        pass


solution = Solution()
assert solution.countNegatives([[4, 3, 2, -1], [3, 2, 1, -1], [1, 1, -1, -2], [-1, -1, -2, -3]]) == 8
