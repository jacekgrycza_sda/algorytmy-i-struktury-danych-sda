# Na podstawie schematu blokowego z prezentacji napisz funcję konwertującą całkowitą liczbę nieujemną do stringa będącą jej reprezentacją binarną

def dec2bin(d):
    pass


assert dec2bin(2) == '10'
assert dec2bin(5) == '101'
assert dec2bin(0) == '0'
assert dec2bin(1) == '1'
assert dec2bin(32) == '100000'
