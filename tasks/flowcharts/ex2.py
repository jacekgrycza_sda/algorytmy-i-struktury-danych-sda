# Na podstawie schematu blokowego z prezentacji, napisz funkcję obliczającą najmniejszą wspólną wielokrotność dwóch liczb całkowitych

def nww(n, m):
    c = m * n
    while (m != n):
        if m > n:
            m = m - n
        else:
            n = n - m
    nww = c / n
    return nww


assert nww(4, 5) == 20
assert nww(10, 10) == 10
assert nww(23, 46) == 46
assert nww(1, 12345) == 12345
