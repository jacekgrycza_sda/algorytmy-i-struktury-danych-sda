from collections import defaultdict

dd = defaultdict(list)
d = {}

sentence = "ala ma kota"

# slownik z kluczem bedacym litera, a wartoscia lista indeksow na ktorych ta litera wystepuje
# 'ala' -> {'a' : [0,2], 'l': [1]}

for idx, ch in enumerate(sentence):
    dd[ch].append(idx)

print(dd)
print(dd['a'])
print(dd['z'])
