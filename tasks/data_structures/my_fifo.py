# Zaimplementuj kolejkę fifo. Wykorzystaj collections.deque
from collections import deque


class FifoQueue:

    def __init__(self):
        self._queue = deque()

    def append(self, element):
        self._queue.append(element)

    def pop(self):
        return self._queue.popleft()


fifo = FifoQueue()
fifo.append(1)
fifo.append(2)
fifo.append(3)
print(fifo.pop())
print(fifo.pop())
print(fifo.pop())
fifo.append(4)
print(fifo.pop())
