# Zaimplementuj książkę telefoniczną jako drzewo binarne BST. Wpis składa się z numeru telefonu i nazwy kontaktu.

class Node:
    def __init__(self, phone_number, user_name, left=None, right=None):
        self.phone_number = phone_number
        self.user_name = user_name
        self.left = left
        self.right = right


class PhoneBook:
    def __init__(self):
        self.root = None

    def insert(self, phone_number, user_name):
        new_node = Node(phone_number, user_name)
        if self.root is None:
            self.root = new_node
        else:
            self.insert_helper(new_node, self.root)

    def insert_helper(self, node, root):
        if node.phone_number < root.phone_number:
            if root.left is None:
                root.left = node
                return
            else:
                self.insert_helper(node, root.left)
        else:
            if root.right is None:
                root.right = node
                return
            else:
                self.insert_helper(node, root.right)

    def find_by_phone_number(self, number):
        return self.find_helper(number, self.root)

    def find_helper(self, number, root):
        if root is None:
            raise ValueError
        elif root.phone_number == number:
            return root.user_name
        elif number < root.phone_number:
            return self.find_helper(number, root.left)
        else:
            return self.find_helper(number, root.right)

    def traverse_in_order(self):
        self.traverse_in_order_helper(self.root)

    def traverse_in_order_helper(self, node):
        if node.left:
            self.traverse_in_order_helper(node.left)
        print(node.phone_number, end=' ')
        if node.right:
            self.traverse_in_order_helper(node.right)

    def traverse_pre_order(self):
        self.traverse_pre_order_helper(self.root)

    def traverse_pre_order_helper(self, node):
        print(node.phone_number, end=' ')
        if node.left:
            self.traverse_in_order_helper(node.left)
        if node.right:
            self.traverse_in_order_helper(node.right)

    def traverse_post_order(self):
        self.traverse_post_order_helper(self.root)

    def traverse_post_order_helper(self, node):
        if node.left:
            self.traverse_in_order_helper(node.left)
        if node.right:
            self.traverse_in_order_helper(node.right)
        print(node.phone_number, end=' ')


phone_book = PhoneBook()
phone_book.insert(5, "Jan")
phone_book.insert(3, "Ala")
phone_book.insert(7, "Witek")
phone_book.insert(1, "Gosia")
phone_book.insert(8, "Ola")
phone_book.insert(4, "Wojtek")
phone_book.insert(6, "Basia")

print("In order:")
phone_book.traverse_in_order()
print()
print("Post order:")
phone_book.traverse_post_order()
print()
print("Pre order:")
phone_book.traverse_pre_order()

# print(phone_book.find_by_phone_number(7))
# print(phone_book.find_by_phone_number(8))
# print(phone_book.find_by_phone_number(5))
# print(phone_book.find_by_phone_number(55))
