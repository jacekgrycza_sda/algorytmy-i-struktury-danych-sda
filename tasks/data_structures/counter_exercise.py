from collections import Counter

favourite_languages = ['PHP', 'Python', 'Java', 'Prolog', 'Elixir', 'Prolog', 'Elixir', 'Prolog', 'Elixir', 'PHP', 'C',
                       'Python', 'Python', 'Python', 'Python', 'Python', 'Python' 'Python', 'Python']

# Wyswietl 3 najbardziej popularne jezyki programowania przy uzyciu Countera

langs_counter = Counter(favourite_languages)
most_popular_langs = langs_counter.most_common(2)
print(most_popular_langs)
print([x[0] for x in most_popular_langs])
print(list(map(lambda x: x[0], most_popular_langs)))
