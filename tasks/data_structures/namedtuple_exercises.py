from collections import namedtuple

Address = namedtuple('Address', ['street', 'house_no', 'postal_code'])
a = Address(street='Mickiewicza', house_no=5, postal_code='44-444')

print(a)
print(a.postal_code)
print(a.house_no)
