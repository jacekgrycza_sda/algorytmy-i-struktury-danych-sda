# Zaimplementuj linked list


class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


class LinkedList:
    def __init__(self, element):
        node = Node(element)
        self.head = node
        self.tail = node
        self.count = 1

    def insert(self, element, idx=None):
        if idx is None or idx == self.count:
            new_node = Node(element)
            if self.head is None:
                self.head = new_node
                self.tail = new_node
            else:
                self.tail.next = new_node
                self.tail = new_node
            self.count += 1
        elif idx > self.count:
            raise IndexError
        elif idx == 0:
            new_node = Node(element, self.head)
            self.head = new_node
            self.count += 1
        else:
            current_idx = 1
            previous_node = self.head
            current_node = self.head.next
            while current_idx < idx:
                previous_node = current_node
                current_node = current_node.next
                current_idx += 1

            new_node = Node(element, current_node)
            previous_node.next = new_node
            self.count += 1

    def delete(self, element):
        if self.head.value == element:
            self.head = self.head.next
            self.count -= 1
            return

        current = self.head.next
        previous = self.head
        while current is not None:
            if current.value == element:
                previous.next = current.next  # do tej pory previous.next == current

                if current == self.tail:
                    self.tail = previous

                self.count -= 1
                return

            previous = current
            current = current.next

        raise ValueError

    def includes(self, element):
        pass

    def get(self, idx):
        pass

    def print_elements(self):
        current = self.head
        print('Length is: ', self.count)
        while current is not None:
            print(f'{current.value}->', end='')
            current = current.next


single_list = LinkedList(1)
single_list.insert(2)
single_list.insert(3)
single_list.insert(4)
single_list.insert(5)
single_list.insert(6)
# single_list.insert(7)
# single_list.insert(8)
# single_list.insert(9)
single_list.print_elements()
single_list.delete(6)
print("\nWithout 6: ", end='')
single_list.print_elements()
single_list.delete(5)
single_list.delete(4)
single_list.delete(2)
single_list.delete(1)
single_list.delete(3)
single_list.print_elements()
print('\nAfter many deletes: ', end='')
single_list.insert(10)
single_list.insert(11)
single_list.insert(12)
single_list.print_elements()
print('')