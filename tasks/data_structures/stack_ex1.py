# Napisz program, który dla podanego stringa sprawdzi, czy nawiasy "(" i ")" są dobrze pootwierane i pozamykane.

def check_parenthesis(input):
    pass


assert check_parenthesis("(2+2)*2")
assert not check_parenthesis(")222222(")
assert not check_parenthesis("(((pp))(")
assert check_parenthesis("ala*(22+(2-2))+(9+1)(9-1)")
assert not check_parenthesis("((())")
assert check_parenthesis("ala ma kota")
