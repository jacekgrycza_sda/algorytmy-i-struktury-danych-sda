# Napisz program, który dla podanego stringa sprawdzi, czy nawiasy okrągłe, kwadratowe i " są poprawnie pootwierane i pozamykane.

opened_list = ['[', '{', '(']
closed_list = [']', '}', ')']
def check_parenthesis(input):
    s = []
    balanced = True
    index = 0
    while index < len(input) and balanced:
        token = input[index]
        if token in opened_list:
            s.append(token)
        elif token in closed_list:
            if len(s) == 0:
                balanced = False
            else:
                s.pop()
        index += 1
    return balanced and len(s) == 0


assert check_parenthesis("(2+2)*2")
assert not check_parenthesis(")222222(")
assert not check_parenthesis("(((pp))(")
assert check_parenthesis("ala*(22+(2-2))+(9+1)(9-1)")
assert not check_parenthesis("((())")
assert check_parenthesis("ala ma kota")
assert check_parenthesis("{{{}}}")
assert check_parenthesis("(){}[]")
assert check_parenthesis("aaaaa(dsadsa)(dsadsa)(!@#$%^&)({{[[[asdasd]]]}})asdasd")
assert not check_parenthesis("aaaaa((dsadsa)(dsadsa)(!@#$%^&)({{[[[asdasd]]]}})asdasd")
assert not check_parenthesis("({[]]})")
assert not check_parenthesis("{]}")
assert not check_parenthesis("((]]")
